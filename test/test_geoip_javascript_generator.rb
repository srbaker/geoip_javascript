require 'minitest/autorun'
require 'minitest/mock'

require 'geoip_javascript_generator'

class TestGeoipJavascriptGenerator < MiniTest::Test
  def test_geoip_lookup
    geoip_lookup_result = { :foo => 'bar', :baz => 'qux' }
    geoip_database_mock = MiniTest::Mock.new.expect(:look_up, geoip_lookup_result, ['24.24.24.24'])

    assert_equal geoip_lookup_result, GeoipJavascriptGenerator.new(geoip_database_mock).look_up('24.24.24.24')
    geoip_database_mock.verify
  end

  def test_javascript_for_ip
    geoip_lookup_result = { :foo => 'bar', :baz => 'qux' }
    geoip_database_mock = MiniTest::Mock.new
    geoip_database_mock.expect(:look_up, geoip_lookup_result, ['24.24.24.24'])
    geoip_database_mock.expect(:look_up, geoip_lookup_result, ['24.24.24.24'])

    expected_javascript1 = /function geoip_foo\(\) \{ return 'bar'; \}/
    expected_javascript2 = /function geoip_baz\(\) \{ return 'qux'; \}/

    assert_match expected_javascript1, GeoipJavascriptGenerator.new(geoip_database_mock).generate_for_ip('24.24.24.24')
    assert_match expected_javascript2, GeoipJavascriptGenerator.new(geoip_database_mock).generate_for_ip('24.24.24.24')
    geoip_database_mock.verify
  end

  def test_javascript_for_ip_should_fail_gracefully_on_nil
    geoip_database_mock = MiniTest::Mock.new.expect(:look_up, nil, ['127.0.0.1'])

    assert_equal '', GeoipJavascriptGenerator.new(geoip_database_mock).generate_for_ip('127.0.0.1')
    geoip_database_mock.verify
  end
end
