require 'minitest/autorun'

require 'javascript_function_generator'

class TestJavascriptFunctionGenerator < MiniTest::Test
  def test_generate_function
    expected = "function prefix_foo() { return 'foo string'; }"
    
    assert_equal expected, JavascriptFunctionGenerator.new('prefix').for_key_value('foo', 'foo string')
  end

  def test_generate_functions_from_hash
    input = { :foo => 'foo string',
      :bar => 'bar string' }

    expected1 = "function prefix_foo() { return 'foo string'; }"
    expected2 = "function prefix_bar() { return 'bar string'; }"

    assert_match expected1, JavascriptFunctionGenerator.new('prefix').for_hash(input)
    assert_match expected2, JavascriptFunctionGenerator.new('prefix').for_hash(input)
  end
  
  def test_do_not_require_a_prefix
    input = { :foo => 'foo string' }

    expected = "function foo() { return 'foo string'; }"

    assert_equal expected, JavascriptFunctionGenerator.new.for_key_value('foo', 'foo string')
  end

  def test_return_empty_string_for_nils
    assert_equal '', JavascriptFunctionGenerator.new.for_hash(nil)
  end
end
