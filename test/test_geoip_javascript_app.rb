require 'minitest/autorun'
require 'minitest/mock'
require 'rack/test'

require 'geoip_app'

ENV['RACK_ENV'] = 'test'

class TestGeoipApp < MiniTest::Test
  include Rack::Test::Methods

  def app
    GeoipApp
  end

  def setup
    app.raise_errors = true
    app.show_exceptions = false
  end    

  def test_loads_the_appropriate_database
    database_mock = MiniTest::Mock.new.expect(:look_up, nil, ['127.0.0.1'])
    database_class_mock = MiniTest::Mock.new.expect(:new, database_mock, ['the_database'])

    app.geoip_database_class = database_class_mock
    app.geoip_database_path = 'the_database'

    get '/'

    database_class_mock.verify
  end

  def test_looks_up_the_correct_ip
    database_mock = MiniTest::Mock.new
    database_class_mock = MiniTest::Mock.new.expect(:new, database_mock, ['the_database'])

    geoip_generator_mock = MiniTest::Mock.new.expect(:generate_for_ip, 'SUCCESS!', ['8.8.4.4'])
    geoip_generator_class_mock = MiniTest::Mock.new.expect(:new, geoip_generator_mock, [database_mock])

    app.geoip_database_path = 'the_database'
    app.geoip_database_class = database_class_mock
    app.geoip_generator_class = geoip_generator_class_mock

    get '/', {}, { 'REMOTE_ADDR' => '8.8.4.4' }

    geoip_generator_mock.verify
  end
end
