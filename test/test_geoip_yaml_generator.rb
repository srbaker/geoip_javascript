require 'minitest/autorun'
require 'minitest/mock'

require 'geoip_yaml_generator'

class TestGeoipYamlGenerator < MiniTest::Test
  def test_geoip_lookup
    geoip_lookup_result = { :foo => 'bar', :baz => 'qux' }
    geoip_database_mock = MiniTest::Mock.new.expect(:look_up, geoip_lookup_result, ['24.24.24.24'])

    assert_equal geoip_lookup_result, GeoipYamlGenerator.new(geoip_database_mock).look_up('24.24.24.24')
    geoip_database_mock.verify
  end

  def test_yaml_for_ip
    geoip_lookup_result = { :foo => 'bar', :baz => 'qux' }
    geoip_database_mock = MiniTest::Mock.new
    geoip_database_mock.expect(:look_up, geoip_lookup_result, ['24.24.24.24'])
    geoip_database_mock.expect(:look_up, geoip_lookup_result, ['24.24.24.24'])

    expected_yaml =<<EOY
---
:geoip:
  :foo: bar
  :baz: qux
EOY

    assert_match expected_yaml, GeoipYamlGenerator.new(geoip_database_mock).generate_for_ip('24.24.24.24')

    geoip_database_mock.verify
  end

  def test_yaml_for_ip_should_fail_gracefully_on_nil
    geoip_database_mock = MiniTest::Mock.new.expect(:look_up, nil, ['127.0.0.1'])

    assert_equal '', GeoipYamlGenerator.new(geoip_database_mock).generate_for_ip('127.0.0.1')
    geoip_database_mock.verify
  end
end
