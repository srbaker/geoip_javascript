require 'yaml'

class GeoipYamlGenerator
  def initialize(geoip_db)
    @geoip_db = geoip_db
  end

  def look_up(ip)
    @geoip_db.look_up(ip)
  end

  def generate_for_ip(ip)
    geoip_hash = @geoip_db.look_up(ip)
    return '' unless geoip_hash
    { geoip: geoip_hash }.to_yaml
  end
end
