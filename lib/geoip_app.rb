require 'sinatra/base'

require 'geoip_javascript_generator'
require 'geoip_city'

class GeoipApp < Sinatra::Application
  configure do
    set :geoip_database_class, GeoIPCity::Database
    set :geoip_database_path, '/usr/share/GeoIP/GeoIP.dat'
    set :geoip_generator_class, GeoipJavascriptGenerator
  end

  get '/' do
    database = settings.geoip_database_class.new(settings.geoip_database_path)
    geoip_generator = settings.geoip_generator_class.new(database)
    geoip_generator.generate_for_ip(request.ip)
  end
end
