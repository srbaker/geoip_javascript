require 'javascript_function_generator'

class GeoipJavascriptGenerator
  def initialize(geoip_db)
    @geoip_db = geoip_db
  end

  def look_up(ip)
    @geoip_db.look_up(ip)
  end

  def generate_for_ip(ip)
    geoip_hash = @geoip_db.look_up(ip)
    JavascriptFunctionGenerator.new('geoip').for_hash(geoip_hash)
  end
end
