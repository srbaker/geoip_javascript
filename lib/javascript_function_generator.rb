class JavascriptFunctionGenerator
  def initialize(prefix=nil)
    @prefix = prefix ? "#{prefix}_" : ''
  end

  def for_key_value(key, value)
    "function #{@prefix}#{key}() { return '#{value}'; }"
  end

  def for_hash(hash)
    return '' unless hash
    hash.collect { |k, v|
      for_key_value(k, v)
    }.join ' '
  end
end
